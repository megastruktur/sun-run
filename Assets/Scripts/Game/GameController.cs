﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	private static GameController _instance;

	[HideInInspector]
	public GameObject player;
  PlayerController playerController;
	MovementController playerMovementController;
  UIController uiController;
  public GameObject spellBook;
  [HideInInspector]
  public SpellBookManager spellBookManager;

	[HideInInspector]
	public GameObject sun;
	Camera mainCamera;
  CameraController mainCameraController;
  private static bool created = false;

	// Pause Menu stuff.
	public GameObject pauseMenu;
	public bool gamePaused;

	public static GameController Instance { get { return _instance; } }

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}

    if (!created) {
      DontDestroyOnLoad(this.gameObject);
      created = true;
    }
	}

	void Start() {
    uiController = GetComponent<UIController> ();
    spellBookManager = spellBook.GetComponent<SpellBookManager> ();
	}

	/**
	 * On Update stuff.
	 */
	void Update() {
    GatherAndLoad ();
		ControlsListener ();
	}

  /**
   * Gather and Load on Update.
   * GameController is not destroyed now.
   *  So gather and load everything is needed
   *  on every level start.
   */
  void GatherAndLoad () {
    if (player == null) {
		  player = GameObject.FindGameObjectWithTag ("Player");
      if (player != null) {
        playerController = player.GetComponent<PlayerController> ();
        playerMovementController = player.GetComponent<MovementController> ();
      }
    }

    if (sun == null) {
		  sun = GameObject.FindGameObjectWithTag ("Sun");
    }

    if (mainCamera == null) {
      mainCamera = Camera.main;
      mainCameraController = mainCamera.GetComponent<CameraController> ();
    }
  }
	
	void FixedUpdate () {
		UIUpdate ();
	}

	/**
	 * UI changes on update.
	 */
	void UIUpdate() {
		uiController.shardsAmountText.text = PlayerController.shardsAmount.ToString();
		uiController.elderSignAmountText.text = PlayerController.elderSignAmount.ToString();
	}

	/**
	 * Listen to misc controls.
	 */
	void ControlsListener() {

		if (KeyPress.PressDown ("Cancel") == 1) {
      PauseToggle ();
		}

    // Show Spellbook
		if (KeyPress.PressDown ("SpellBook") == 1) {
      spellBookManager.SpellBookToggle ();
		}
	}

	/**
	 * Make a Pause.
	 */
	public void Pause(bool showMenu = true) {
		Time.timeScale = 0;
    if (showMenu) {
		  pauseMenu.SetActive (true);
    }
		gamePaused = true;
	}

	/**
	 * Unpause.
	 */
	public void Unpause(bool hideMenu = true) {
		Time.timeScale = 1f;
    if (hideMenu) {
		  pauseMenu.SetActive (false);
    }
		gamePaused = false;
	}

  public void PauseToggle (bool menu = true) {
    if (gamePaused) {
      Unpause (menu);
    } else {
      Pause (menu);
    }
  }

	/**
	 * Restart the level.
	 */
	public void Restart() {
		Scene currentSceneId = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (currentSceneId.buildIndex);
    GameController.ReloadStartSceneVars ();
    uiController.YouDiedUIOff ();
		Unpause ();
	}

	/**
	 * Go to Main Menu.
	 */
	public void MainMenu() {
		SceneManager.LoadScene (0);
		Unpause ();
	}

  /**
   * Spellcast Visuals and some logic.
   */
  public void SpellCastMode(bool enabled) {
    mainCameraController.SpellCastEffect(enabled);
    uiController.magicCircleCast.SetActive(enabled);
    if (enabled) {
      Time.timeScale = 0.25f;
    } else {
      Time.timeScale = 1f;
    }
  }

  public void YouDied () {
    mainCameraController.SpellCastEffect(true);
    uiController.YouDiedUI ();
    player.SetActive (false);
  }

  public static void LoadLevel (string name) {
    PlayerController.shardsAmountStart = PlayerController.shardsAmount;
    PlayerController.elderSignAmountStart = PlayerController.elderSignAmount;
    OverheatController.overheatLevelStart = OverheatController.overheatLevel;
    SceneManager.LoadScene (name);
  }

  public static void ClearVars () {
    PlayerController.shardsAmount = 0;
    PlayerController.elderSignAmount = 0;
    OverheatController.overheatLevel = 0f;
  }

  public static void ReloadStartSceneVars () {
    PlayerController.shardsAmount = PlayerController.shardsAmountStart;
    PlayerController.elderSignAmount = PlayerController.elderSignAmountStart;
    OverheatController.overheatLevel = OverheatController.overheatLevelStart;
  }

  void OnLevelWasLoaded(int level) {

    // After Restarting the level (Game Controller stills, camera destroyed)
    //  render camera reference is destroyed.
    //  This will reenable it.
    if (uiController != null) {
      Canvas canv = uiController.UI.GetComponent<Canvas> ();
      canv.worldCamera = Camera.main;
    }
  }
}
