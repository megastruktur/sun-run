﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

	public Slider overheatLevel;
	public Text shardsAmountText;
	public Text elderSignAmountText;

  public GameObject magicCircleCast;

  public GameObject youDied;

	[HideInInspector]
	public Animator overheatLevelAnim;

  public GameObject UI;

	private static UIController _instance;
	public static UIController Instance { get { return _instance; } }

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		overheatLevelAnim = overheatLevel.GetComponent<Animator> ();
		YouDiedUIOff ();
	}

  public void YouDiedUI () {
    youDied.SetActive(true);
  }

  public void YouDiedUIOff () {
    youDied.SetActive(false);
  }
}
