﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Axis Key down presser.
 */
public class KeyPress : MonoBehaviour {

	static KeyPress _instance;

	private static Dictionary<string, bool> keyDowns;
	private float keyTimeout = 1f;

	public static KeyPress Instance { get { return _instance; } }

	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	void Start() {
		keyDowns = new Dictionary<string, bool> ();
	}

	/**
	 * Press Down returns a non-zero value only once.
	 */
	public static float PressDown(string axisName) {

		bool down = false;
		if (!keyDowns.TryGetValue (axisName, out down)) {
			keyDowns.Add(axisName, down);
		}

		// Press logic here.
		float axisValue = Input.GetAxisRaw (axisName);

		if (axisValue != 0f) {
			if (!keyDowns [axisName]) {
				keyDowns [axisName] = true;
				return axisValue;
			}
		} else {
			keyDowns [axisName] = false;
		}

		// Return 0 if nothing pressed.
		return 0f;
	}

}
