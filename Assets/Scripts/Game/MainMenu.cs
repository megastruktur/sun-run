﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	public void NewGameStart() {
    GameController.ClearVars ();
		SceneManager.LoadScene (1);
	}
}
