﻿using UnityEngine;

public class VectorDrag : MonoBehaviour {
  
  [HideInInspector]
  public Vector3 position;
  [HideInInspector]
  public float handleSize = 0.25f;

  void OnDrawGizmos() {
    // Debug.Log (Tools.handlePosition);
		Gizmos.color = Color.yellow;

    if (position == null) {
      position = transform.position;
    }
    Gizmos.DrawCube(position, new Vector2(handleSize, handleSize));
    Gizmos.DrawLine (transform.position, position);
  }

}