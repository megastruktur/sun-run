﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour {

	public virtual void Collect(PlayerController playerController) {
		gameObject.SetActive (false);
	}
}
