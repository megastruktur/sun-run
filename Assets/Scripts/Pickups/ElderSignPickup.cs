﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElderSignPickup : Pickup {

	public override void Collect(PlayerController playerController) {
		base.Collect (playerController);
		PlayerController.elderSignAmount++;
	}

}
