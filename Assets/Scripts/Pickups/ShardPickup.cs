﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShardPickup : Pickup {

	public override void Collect(PlayerController playerController) {
		base.Collect (playerController);
		PlayerController.shardsAmount++;
	}

}
