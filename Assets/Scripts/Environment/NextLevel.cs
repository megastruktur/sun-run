﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class NextLevel : MonoBehaviour {

  public Object nextScene;

	// Use this for initialization
	void Start () {
		
	}

	/**
	 * When collided - load next scene.
	 */
	void OnTriggerEnter2D(Collider2D c) {
		if (c.gameObject.CompareTag ("Player")) {
			LoadNext ();
		}
	}

  void LoadNext () {
    GameController.LoadLevel (nextScene.name);
  }

}
