﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class MovingPlatform : TwoPointMovable {
  
	protected BoxCollider2D trigger;

	/**
	 * When collided - make it parent to a player to move together.
	 */
	void OnTriggerEnter2D(Collider2D c) {
		if (c.gameObject.CompareTag ("Player")) {
			c.gameObject.transform.parent = transform;
		}
	}

	void OnTriggerExit2D(Collider2D c) {
		if (c.gameObject.CompareTag ("Player")) {
			c.gameObject.transform.parent = null;
		}
	}
}
