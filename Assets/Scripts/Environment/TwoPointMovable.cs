﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPointMovable : VectorDrag {


  protected Vector3 fromPosition;
  public float speed = 0.5f;
  protected float i;

  protected Queue<Vector3> nextPosition;

	// Use this for initialization
	virtual protected void Start () {
    fromPosition = transform.position;
    nextPosition = new Queue<Vector3> ();
    nextPosition.Enqueue (position);
    nextPosition.Enqueue (fromPosition);
	}

	// Update is called once per frame
	void FixedUpdate () {
    Move ();
	}

  protected void Move () {

    float step = speed * Time.deltaTime;
    i += speed * Time.deltaTime;

    transform.position = Vector3.Lerp (fromPosition, nextPosition.Peek (), i);

    // Change target moving point and flip.
    if (transform.position == nextPosition.Peek ()) {
      fromPosition = nextPosition.Dequeue ();
      nextPosition.Enqueue (fromPosition);
      i = 0;
      Flip ();
    }
  }

  protected virtual void Flip () {}
}
