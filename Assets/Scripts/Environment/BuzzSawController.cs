﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BuzzSawController : TwoPointMovable {

  protected Animator anim;

	// Use this for initialization
	override protected void Start () {
    base.Start();
    anim = GetComponent<Animator> ();
	}

  override protected void Flip () {

    bool currentFlip = ((fromPosition.x - nextPosition.Peek ().x) >= 0);
    if (currentFlip) {
      anim.SetBool ("invert", false);
    } else {
      anim.SetBool ("invert", true);
    }
  }
}
