﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HazardCollider : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D c) {
		if (c.gameObject.CompareTag ("Player")) {
			print ("Hazard");
			GameController.Instance.YouDied ();
		}

	}

}
