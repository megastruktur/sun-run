﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour {

	public float speed = 2.5f;
	[HideInInspector]
	GameObject player;

	void Start() {
	}

	// Update is called once per frame
	void Update () {

		if (player == null && GameController.Instance.player != null) {
			player = GameController.Instance.player;
		}

    if (player) {
      Vector2 pos = transform.position;
      pos.x += speed * Time.deltaTime;
      pos.y = player.transform.position.y;
      transform.position = pos;
    }
	}
}
