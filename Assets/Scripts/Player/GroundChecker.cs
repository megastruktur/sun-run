﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
  [HideInInspector]
  public bool isGrounded;
  [HideInInspector]
  public GameObject currentGround;
  [SerializeField]
  private LayerMask whatIsGround;
  [SerializeField]
  private float radius = 1f;

	// Gizmos and ingame debug.
	public Color debugColor = Color.red;

	void FixedUpdate() {
		Collider2D[] groundHits = Physics2D.OverlapCircleAll(transform.position, radius, whatIsGround);
		DebugExtension.DebugCircle(transform.position, Vector3.back, debugColor, radius);

    isGrounded = false;
    currentGround = null;

    foreach (var groundHit in groundHits) {
      if (!groundHit.isTrigger) {
        isGrounded = true;
        currentGround = groundHit.gameObject;
        break;
      }
    }        
  }

	/**
	 * Let's Draw some gizmos  :)
	 */
	void OnDrawGizmosSelected() {
		Gizmos.color = debugColor;
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}
