﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Spell {
  public string name;
  public string description;
  public string callbackName;
  public string keys;
  public int overheatMultiplier = 0;

}
