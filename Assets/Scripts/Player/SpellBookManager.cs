﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellBookManager : MonoBehaviour {

  public Transform pageLeft;
  public Transform pageRight;

  SpellWordManager spellWordManager;
  public Transform spellInBook;
  public bool spellBookOpen = false;
  Renderer rend;

  private static SpellBookManager _instance;
	public static SpellBookManager Instance { get { return _instance; } }
	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	// Use this for initialization
	void Start () {
		spellWordManager = SpellWordManager.Instance;
    rend = GetComponent<Renderer> ();
	}
	
  public void FillTheSpell (Spell spell) {
    Transform spellInBookObject = Instantiate (spellInBook, pageLeft);
    SpellInBookController sc = spellInBookObject.gameObject.GetComponent<SpellInBookController> ();
    sc.keys.text = spell.keys;
    sc.spellName.text = spell.name;
    sc.description.text = spell.description;
  }

  public void SpellBookToggle () {
    
    bool isAlreadyPaused = (GameController.Instance.gamePaused);
    bool isSpellModeOn = (SpellWordManager.Instance.spellModeOn);

    if (!isSpellModeOn && !(isAlreadyPaused && !spellBookOpen)) {
      spellBookOpen = !spellBookOpen;
      gameObject.SetActive (spellBookOpen);
      GameController.Instance.PauseToggle (false);
    }
  }

  public void ClearSpellBook () {
    foreach (Transform child in pageLeft) {
      Destroy (child.gameObject);
    }
  }
}
