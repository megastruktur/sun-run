﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

public class SpellWordManager : MonoBehaviour {

	Dictionary<string, Spell> spellWords;
  SpellCaster spellCaster;
  Type spellCasterType;
  SpellWordVisuals spellWordVisuals;
  private static SpellWordManager _instance;

  OverheatController overheatController;

	[HideInInspector]
	public bool spellModeOn = false;
	[HideInInspector]
	public string spellword;

	public static SpellWordManager Instance { get { return _instance; } }
	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	private void Start() {
		spellWords = new Dictionary<string, Spell> ();
    spellCaster = GetComponent<SpellCaster> ();
    spellCasterType = typeof(SpellCaster);
    overheatController = GetComponent<OverheatController> ();
    spellWordVisuals = SpellWordVisuals.Instance;
    
    SpellWordsLoad();
	}

  public void AddToSpellbook (string keymap, Spell spell) {
		spellWords.Add(keymap, spell);
    GameController.Instance.spellBookManager.FillTheSpell (spell);
  }

  private void SpellWordsLoad() {

    // Jump Amount mod
    // Move Speed mod
    // Feather Fall mod
    // Full Cooldown mod
    // Overheat Slower mod

    Spell lightCooldownSpell = new Spell ();
    lightCooldownSpell.name = "Light Cooldown";
    lightCooldownSpell.description = "Cooldown the 50% of Overheat level scale";
    lightCooldownSpell.keys = "wszxcedc";
    lightCooldownSpell.callbackName = "LightCooldown";

    Spell jumperSpell = new Spell ();
    jumperSpell.name = "Jumper";
    jumperSpell.description = "Make high jump";
    jumperSpell.keys = "tgbfgh";
    jumperSpell.callbackName = "Jumper";

    Spell jetSpell = new Spell ();
    jetSpell.name = "Jet";
    jetSpell.description = "Jet into the air 45 deg, movement direction";
    jetSpell.keys = "cft";
    jetSpell.callbackName = "Jet";
    jetSpell.overheatMultiplier = 2;

    GameController.Instance.spellBookManager.ClearSpellBook ();
		AddToSpellbook(lightCooldownSpell.keys, lightCooldownSpell);
    AddToSpellbook(jumperSpell.keys, jumperSpell);
    AddToSpellbook(jetSpell.keys, jetSpell);

  }

	private void Update() {
		spellModeControl();
		typingControl();
	}

	/**
		* We need to control SpellMode.
		*/
	private void spellModeControl() {
		if (!GameController.Instance.gamePaused && KeyPress.PressDown ("SpellMode") == 1f) {
			spellModeOn = !spellModeOn;
      GameController.Instance.SpellCastMode(spellModeOn);

      if (!spellModeOn) {
		    castSpell();
      }
		}
	}

	/**
	 * What happens on typing.
	 */
	private void typingControl() {
		if (spellModeOn && Input.inputString.ToLower().Length > 0) {
      string letter = Input.inputString.ToLower();
			spellword += letter;
      if (letter == " ") {
        spellWordVisuals.AddSpellCanvas ();
      } else {
        spellWordVisuals.AddLetterPoint (letter);
      }
		}
	}

  /**
   * Cast a spell.
   * @todo Remove validations: all spells should exist anyway
   */
	private void castSpell() {
  
    // If spellMode just disabled (spellmode 0 and
    //  spellword not empty)
    // Clear spell visuals.
    spellWordVisuals.Clear ();
    
    // Cast a spell!
    if (spellword.Length > 0) {
      string[] spellwordsSplit = spellword.Split(' ');

      foreach (string singleSpellword in spellwordsSplit) {
        // Get the Spell Method name.
        Spell spell = null;
        if (spellWords.TryGetValue(singleSpellword, out spell)) {

          // Validate whether method exists
          MethodInfo spellMethod = spellCasterType.GetMethod(spell.callbackName);
          if (spellMethod != null) {

            // Invoke if all is good.
            overheatController.GainAmountMulti (spell.overheatMultiplier);
            spellCaster.Invoke(spell.callbackName, 0);
          }
        }
      }
      spellword = "";
    }
	}

}
