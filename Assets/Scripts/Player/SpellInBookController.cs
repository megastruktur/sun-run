﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellInBookController : MonoBehaviour {

  public Text keys;
  public Text spellName;
  public Text description;
}
