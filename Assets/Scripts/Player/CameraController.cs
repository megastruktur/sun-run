﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;
public class CameraController : MonoBehaviour {

	GameObject player;
  [HideInInspector]
  public Transform lookAt;
	public float zDistance = -10f;
	VignetteAndChromaticAberration vignetteEffect;
	float vignetteLevel = 0.3f;
	float vignetteEffectSpeed = 0.05f;

	IEnumerator spellcastCoroutine;
  Coroutine spellcastCoroutineStarted;

  public float boundX = 1.2f;
  public float boundY = 1.0f;
  float cameraSpeed = 0.15f;

	private void Start() {
		vignetteEffect = GetComponent<VignetteAndChromaticAberration>();
    player = GameObject.FindGameObjectWithTag ("Player");
    lookAt = player.transform;
	}

	
	void LateUpdate () {

    Vector3 delta = Vector3.zero;

    // X Axix.
    float dx = lookAt.position.x - transform.position.x;
    if (dx > boundX || dx < -boundX) {
      if (transform.position.x < lookAt.position.x) {
        delta.x = dx - boundX;
      } else {
        delta.x = dx + boundX;
      }
    }

    // Y Axis
    float dy = lookAt.position.y - transform.position.y;
    if (dy > boundY || dy < -boundY) {
      if (transform.position.y < lookAt.position.y) {
        delta.y = dy - boundY;
      } else {
        delta.y = dy + boundY;
      }
    }
    Vector3 desiredPosition = transform.position + delta;
    desiredPosition.z = zDistance;
    transform.position = Vector3.Lerp (transform.position, desiredPosition, cameraSpeed);
	}

	/**
	* Enable or disable SpellCast Mode.
	*/
	public void SpellCastEffect(bool enable) {

		if (spellcastCoroutineStarted != null) {
			StopCoroutine(spellcastCoroutineStarted);
		}
		spellcastCoroutine = SpellCastEffectCoroutine(enable);
		spellcastCoroutineStarted = StartCoroutine(spellcastCoroutine);
	}

	/**
	 * Start SpellCast Camera effect.
	 */
	IEnumerator SpellCastEffectCoroutine(bool enable) {

		if (enable) {
			while (vignetteEffect.intensity < vignetteLevel) {
				vignetteEffect.intensity += vignetteEffectSpeed;
				if (vignetteEffect.intensity > vignetteLevel) {
					vignetteEffect.intensity = vignetteLevel;
				}
				yield return null;
			}
		} else {

			while (vignetteEffect.intensity > 0f) {
				vignetteEffect.intensity -= vignetteEffectSpeed;
				if (vignetteEffect.intensity <= 0f) {
					vignetteEffect.intensity = 0f;
				}
				yield return null;
			}

		}
	}

}
