﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellWordVisuals : MonoBehaviour {

  private static SpellWordVisuals _instance;

  LineRenderer spellEffectRenderer;
  SpellWordManager spellWordManager;
  Dictionary<string, Vector2> letters;

  Vector2 defaultCanvasSize;
  List<GameObject> spellCanvases;
  float canvasSpacing;
  float canvasMarginMultiplier;

  float spellLineWidth;
  Color spellLineColor;

	public static SpellWordVisuals Instance { get { return _instance; } }
	private void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(this.gameObject);
		} else {
			_instance = this;
		}
	}

	// Use this for initialization
	void Start () {

    defaultCanvasSize = new Vector2 (2f, 1f);
    spellCanvases = new List<GameObject> ();
    spellLineWidth = 0.1f;
    spellLineColor = Color.red;
    canvasSpacing = 0f;
    canvasMarginMultiplier = 50f;

    KeyboardCoordinatesSetup ();
	}
	
  public void AddSpellCanvas () {

    int canvasesAmount = spellCanvases.Count;
    GameObject newCanvas = new GameObject("symbol" + canvasesAmount, typeof(RectTransform));
    spellEffectRenderer = newCanvas.AddComponent (typeof(LineRenderer)) as LineRenderer;
    LineRendSetup ();

    newCanvas.transform.parent = transform;
    float marginLeft = (canvasesAmount * defaultCanvasSize.x + canvasesAmount * canvasSpacing) * canvasMarginMultiplier;

    Vector2 canvasPosition = new Vector2 (marginLeft, 0f);

    RectTransform rT = newCanvas.GetComponent<RectTransform> ();

    spellCanvases.Add (newCanvas);
  }

  /**
   * Add a letter to LineRenderer.
   */
  public void AddLetterPoint (string key) {

    if (spellEffectRenderer == null) {
      AddSpellCanvas ();
    }

    Vector2 keyCoord;
    if (letters.TryGetValue(key, out keyCoord)) {
      float x_point = keyCoord.x * defaultCanvasSize.x / 100;
      float y_point = keyCoord.y * defaultCanvasSize.y / 100;
      int positionCount = spellEffectRenderer.positionCount + 1;
      spellEffectRenderer.positionCount = positionCount;
      spellEffectRenderer.SetPosition(positionCount - 1, new Vector2(x_point, y_point));
    }
  }

  /**
   * Clear all canvases.
   */
  public void Clear() {
    foreach (GameObject canv in spellCanvases) {
      Destroy(canv);
    }
    spellCanvases.Clear ();
  }

  void KeyboardCoordinatesSetup() {

    letters = new Dictionary<string, Vector2> ();
    float firstRow = 0f;
    float secondRow = -40f;
    float thirdRow = -80f;
    letters.Add ("q", new Vector2(0f, firstRow));
    letters.Add ("w", new Vector2(10f, firstRow));
    letters.Add ("e", new Vector2(20f, firstRow));
    letters.Add ("r", new Vector2(30f, firstRow));
    letters.Add ("t", new Vector2(40f, firstRow));
    letters.Add ("y", new Vector2(50f, firstRow));
    letters.Add ("u", new Vector2(60f, firstRow));
    letters.Add ("i", new Vector2(70f, firstRow));
    letters.Add ("o", new Vector2(80f, firstRow));
    letters.Add ("p", new Vector2(90f, firstRow));

    letters.Add ("a", new Vector2(3f, secondRow));
    letters.Add ("s", new Vector2(13f, secondRow));
    letters.Add ("d", new Vector2(23f, secondRow));
    letters.Add ("f", new Vector2(33f, secondRow));
    letters.Add ("g", new Vector2(43f, secondRow));
    letters.Add ("h", new Vector2(53f, secondRow));
    letters.Add ("j", new Vector2(63f, secondRow));
    letters.Add ("k", new Vector2(73f, secondRow));
    letters.Add ("l", new Vector2(83f, secondRow));

    letters.Add ("z", new Vector2(8f, thirdRow));
    letters.Add ("x", new Vector2(18f, thirdRow));
    letters.Add ("c", new Vector2(28f, thirdRow));
    letters.Add ("v", new Vector2(38f, thirdRow));
    letters.Add ("b", new Vector2(48f, thirdRow));
    letters.Add ("n", new Vector2(58f, thirdRow));
    letters.Add ("m", new Vector2(68f, thirdRow));
  }

  void LineRendSetup () {
    // SetVertexCount (length)
    spellEffectRenderer.useWorldSpace = false;
    spellEffectRenderer.startColor = spellLineColor;
    spellEffectRenderer.endColor = spellLineColor;
    spellEffectRenderer.material = new Material(Shader.Find("Sprites/Default"));
    spellEffectRenderer.startWidth = spellLineWidth;
    spellEffectRenderer.endWidth = spellLineWidth;
    spellEffectRenderer.positionCount = 0;
    spellEffectRenderer.sortingLayerName = "UITop";
    // spellEffectRenderer.numCornerVertices = 2;
    // spellEffectRenderer.numCapVertices = 5;
    // spellEffectRenderer.sortingOrder = 1;
  }

  public void ClearSpellUI () {
    // foreach (Transform child in transform) {
    //   Destroy (child.gameObject);
    // }
  }
}
