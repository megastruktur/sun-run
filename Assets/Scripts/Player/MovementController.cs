﻿/**
 * We count overcharge by dividing 100 by amount of letters.
 * So we can balance the game by counting how many phrases per scale per level we need.
 * 
 * @todo Movement animation on overheat cancel.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {
	
	Rigidbody2D rigid;
	Animator anim;
	GameController gameController;
	SpriteRenderer sprite;
	OverheatController overheatController;
  SpellWordManager spellWordManager;

	// Movement props.
	float movementHorizontal;
	float speed;
	float jumpThrust;
  float jumpThrustDefault;
	bool stop = false;
  [HideInInspector]
  public int jumpAmountDefault;
  int jumpAmount;
  float fallMultiplier;
  float lowJumpMultiplier;

	[HideInInspector]
	public bool movementAllowed;

  public GroundChecker groundChecker;

	// Use this for initialization
	void Start () {

		movementAllowed = true;
		speed = 4f;
		jumpThrust = 5f;
    jumpThrustDefault = 5f;
    fallMultiplier = 1.5f;
    lowJumpMultiplier = 2f;
    jumpAmountDefault = 2;
    jumpAmount = jumpAmountDefault;

		rigid = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		gameController = GameController.Instance;
		sprite = GetComponent<SpriteRenderer> ();
		overheatController = GetComponent<OverheatController> ();
    spellWordManager = GetComponent<SpellWordManager> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		JumpControls ();
	}

	void Update() {
		MovementAllowed ();
		MovementControls ();
    RealisticFall ();
	}

	/**
	 * Movement Allowed checker.
	 */
	void MovementAllowed() {
		movementAllowed = !spellWordManager.spellModeOn && !stop && !overheatController.overheat;
	}

	/**
	 * Movement Controls pressed.
	 */
	void MovementControls() {
    if (movementAllowed && Input.GetAxisRaw ("Horizontal") != 0) {
			movementHorizontal = Input.GetAxisRaw ("Horizontal");
      MoveAction ();
    } else {
      IdleAction ();
		}
	}

	/**
	 * Move Mechanics.
	 */
	void MoveAction() {
		if (movementHorizontal >= 0) {
			sprite.flipX = false;
		} else {
			sprite.flipX = true;
		}
			
		anim.SetInteger ("State", 1);
		float x_speed = speed * movementHorizontal;
		Vector2 moveVelocity = new Vector2 (x_speed, rigid.velocity.y);
		rigid.velocity = moveVelocity;
		overheatController.Gain();
	}

	/**
	 * Stay at one place action.
	 */
	void IdleAction() {
		anim.SetInteger ("State", 0);
	}

	/**
	 * Jump Pressed controller.
	 */
	void JumpControls() {

    // We need to compare to jumpAmountDefault because jumpAmount
    //  may be modified by conditions.
    if (jumpAmount < jumpAmountDefault && groundChecker.isGrounded) {
      jumpAmount = jumpAmountDefault;
    }

		if (movementAllowed) {
			bool jumpAllowed = (jumpAmount > 0);

			if (KeyPress.PressDown ("Jump") == 1 && jumpAllowed == true) {
				JumpAction ();
        jumpAmount--;
			}
		}

	}

	/**
	 * Jump action.
	 */
	void JumpAction() {
    rigid.velocity = Vector2.up * jumpThrust;
		anim.SetTrigger ("Jump");
		overheatController.GainAmountMulti (10);
    jumpThrust = jumpThrustDefault;
	}

  /**
   * Modify one jump's thrust.
   */
  public void JumpMod (float thrustMod) {
    jumpThrust = jumpThrust * thrustMod;
  }

  void RealisticFall () {
    if (rigid.velocity.y < 0) {
      rigid.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
    } else if (rigid.velocity.y > 0 && !Input.GetButton ("Jump")) {
      rigid.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
    }
  }

  public void Jet () {
    int flipMod = (sprite.flipX == true) ? -1 : 1;
    Vector2 jetForce = (Vector2.right * flipMod + Vector2.up) * 400f;
    rigid.AddRelativeForce (jetForce);
  }
}
