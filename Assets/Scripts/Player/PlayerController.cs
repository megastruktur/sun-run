﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[HideInInspector]
	public static int shardsAmount = 0;
	[HideInInspector]
	public static int elderSignAmount = 0;

	[HideInInspector]
	public static int shardsAmountStart = 0;
	[HideInInspector]
	public static int elderSignAmountStart = 0;

  OverheatController overheatController;
  SpellWordManager spellwordManager;

	private void Start() {
    overheatController = GetComponent<OverheatController> ();
	}


	void OnTriggerEnter2D(Collider2D c) {
		if (c.gameObject.CompareTag ("Pickup")) {
      c.gameObject.GetComponent<Pickup> ().Collect(this);
		}
	}
}
