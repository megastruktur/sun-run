﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCaster : MonoBehaviour {

  OverheatController overheatController;
  MovementController movementController;

  private void Start() {
    overheatController = GetComponent<OverheatController> ();
    movementController = GetComponent<MovementController> ();
  }

  public void LightCooldown () {
    overheatController.LoseAmount(50f);
    Debug.Log("Cool, cool!");
  }

  public void Cross () {
    Debug.Log("Cross");
  }
  
  public void Circle () {
    Debug.Log("Circle");
  }

  public void Jumper () {
    movementController.JumpMod (2f);
    Debug.Log ("High Jump");
  }

  public void Jet () {
    movementController.Jet ();
    Debug.Log ("Jet");
  }
}
