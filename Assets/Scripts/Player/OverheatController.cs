﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverheatController : MonoBehaviour {

	GameController gameController;
  UIController uiController;

	[HideInInspector]
	public static float overheatLevel;

	[HideInInspector]
	public static float overheatLevelStart;
	float overheatDischargeSpeed;
  [HideInInspector]
	public float overheatGainSpeed;
	[HideInInspector]
	public bool overheat;

	public ParticleSystem overheatParticle;
	bool overheatParticleStarted;

	// Use this for initialization
	void Start () {
		overheatDischargeSpeed = 0.5f;
		overheatGainSpeed = 0.3f;
		overheat = false;
		overheatParticleStarted = false;
		gameController = GameController.Instance;
    uiController = UIController.Instance;
    overheatLevelStart = 0f;
	}
	

	void FixedUpdate () {
		OverheatCtrl ();
	}

	void OverheatCtrl () {

		if (overheatLevel > 0) {

			if (overheat == false && overheatLevel >= 100f) {
				OverheatAction ();
			} else {
				if (overheatLevel < 100) {
					OverheatCooldowned ();
				}
			}
		} else {
			OverheatNull ();
		}
		uiController.overheatLevel.value = overheatLevel;
	}

	public void Gain () {
		overheatLevel += overheatGainSpeed;
	}
	public void GainAmountMulti (int gainAmountMultiplier) {
		overheatLevel += (overheatGainSpeed * gainAmountMultiplier);
	}

  public void Lose () {
		overheatLevel -= overheatDischargeSpeed;
  }

  /**
   * Lose amount of Overheat.
   */
  public void LoseAmount (float amountPercent) {
    StartCoroutine(LoseAmountCoroutine(amountPercent));
  }


	/**
	 * Overheat Action.
	 */
	void OverheatAction() {
		// Starting overheat particle only once.
		if (!overheatParticleStarted) {
			overheatParticle.Play ();
			overheatParticleStarted = true;
			//var sz = overheatParticle.sizeOverLifetime;
			//sz.enabled = true;
			//sz.sizeMultiplier = 20;
		}

		overheatLevel = 100f;
		overheat = true;
		StartCoroutine ("Overheat");
	}

	/**
	 * Nullify the overheat.
	 */
	public void OverheatNull() {
		overheatParticle.Stop ();
		overheatParticleStarted = false;
		overheatLevel = 0;
		overheat = false;
		OverheatAnimationStop ();
	}

	/**
	 * Overheat is cooling down.
	 */
	public void OverheatCooldowned() {
		overheatParticle.Stop ();
		overheatParticleStarted = false;
		overheat = false;
		OverheatAnimationStop ();
	}

	/**
	 * Emergency Stop the Overheat animation.
	 */
	private void OverheatAnimationStop() {
		StopCoroutine ("Overheat");
		uiController.overheatLevelAnim.SetInteger ("State", 0);
	}

	/**
	 * Overheat Animation.
	 */
	IEnumerator Overheat() {
		
		while (overheat) {
			uiController.overheatLevelAnim.SetInteger ("State", 1);
			yield return null;
		}

		uiController.overheatLevelAnim.SetInteger ("State", 0);

	}

  IEnumerator LoseAmountCoroutine (float amountPercent) {
    while (amountPercent > 0) {
      overheatLevel -= overheatDischargeSpeed;
      amountPercent -= overheatDischargeSpeed;
      yield return null;
    }
  }
}
