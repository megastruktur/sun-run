﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(VectorDrag), true)]
public class VectorDragEditor : Editor
{
    float size = 0.25f;
    bool editorMode = false;

    protected virtual void OnSceneGUI() {

        VectorDrag vd = (VectorDrag)target;
        Vector3 snap = Vector3.one * 0.5f;

        EditorGUI.BeginChangeCheck();
        Vector3 newposition = Handles.FreeMoveHandle(
          vd.position,
          Quaternion.identity,
          vd.handleSize,
          snap,
          Handles.RectangleHandleCap
        );
        
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(vd, "Change Look At Target Position");
            vd.position = newposition;
        }
    }
}